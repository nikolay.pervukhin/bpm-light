package net.pervukhin.bpmlight.configuration;

import net.pervukhin.bpmlight.delegate.RestDelegate;
import net.pervukhin.bpmlight.service.CustomHistoryHandler;
import net.pervukhin.bpmlight.service.ExporterRegistryService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.cfg.StandaloneInMemProcessEngineConfiguration;
import org.camunda.bpm.engine.impl.persistence.StrongUuidGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class ProcessEngineConfiguration {

    @Bean
    public ProcessEngine processEngine(@Autowired RestDelegate restDelegate,
                                       @Autowired ExporterRegistryService exporterRegistryService) {
        final StandaloneInMemProcessEngineConfiguration config = new StandaloneInMemProcessEngineConfiguration();
        // используем полное логирование процесса, full - включая переменные
        config.setHistory("full");
        config.setDatabaseSchemaUpdate("create");
        // отключаем сбор метрик
        config.setMetricsEnabled(false);
        config.setDbMetricsReporterActivate(false);
        // Добавление бина делегата
        config.setBeans(new HashMap<>());
        config.getBeans().put("restDelegate", restDelegate);
        // Обработчик событий процесса (старт процесса, этапы процесса, конец процесса, изменение переменных и тп)
        config.setHistoryEventHandler(new CustomHistoryHandler(exporterRegistryService));
        config.setIdGenerator(new StrongUuidGenerator());
        return config.buildProcessEngine();
    }
}
