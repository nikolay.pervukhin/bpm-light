package net.pervukhin.bpmlight.exporter.clickhouse.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.clickhouse.ClickhouseConnectionService;
import net.pervukhin.bpmlight.model.Variable;
import net.pervukhin.bpmlight.repository.VariableRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service("variableRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "clickhouse")
public class VariableRepository implements VariableRepositoryInt {
    @Autowired
    private ClickhouseConnectionService databaseConnectionService;

    private final VariableMapper variableMapper = new VariableMapper();

    @Override
    public PaginationContainerDTO<Variable> getList(String processInstanceId, String maxResults, String firstResults) {
        return new PaginationContainerDTO<>(
                databaseConnectionService.getJdbcTemplate().queryForObject(
                        "select count() from bpmlight.variable where processInstanceId=?", Integer.class,
                        processInstanceId),
                databaseConnectionService.getJdbcTemplate().query(
                        "select * from bpmlight.variable where processInstanceId=? order by created "
                                + "limit ? offset ?",
                        variableMapper, processInstanceId, Integer.valueOf(maxResults), Integer.valueOf(firstResults)
                )
        );
    }

    class VariableMapper implements RowMapper<Variable> {
        @Override
        public Variable mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Variable variable = new Variable();
            variable.setDate(rs.getTimestamp("created"));
            variable.setProcessInstanceId(rs.getString("processInstanceId"));
            variable.setVariableName(rs.getString("variableName"));
            variable.setTextValue(rs.getString("textValue"));
            variable.setSerializerName(rs.getString("serializerName"));
            return variable;
        }
    }
}
