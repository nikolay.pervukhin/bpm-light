package net.pervukhin.bpmlight.exporter.clickhouse.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.clickhouse.ClickhouseConnectionService;
import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.repository.DeploymentRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service("deploymentRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "clickhouse")
public class DeploymentRepository implements DeploymentRepositoryInt {
    @Autowired
    private ClickhouseConnectionService databaseConnectionService;

    private final DeploymentMapper deploymentMapper = new DeploymentMapper();

    @Override
    public PaginationContainerDTO<Deployment> getList(String deploymentKey, String maxResults, String firstResults) {
        return new PaginationContainerDTO<>(
                databaseConnectionService.getJdbcTemplate().queryForObject(
                        "select count() from bpmlight.deployment where deploymentKey=?", Integer.class, deploymentKey),
                databaseConnectionService.getJdbcTemplate().query(
                        "select * from bpmlight.deployment where deploymentKey=? order by created limit ? offset ?",
                        deploymentMapper, deploymentKey, Integer.parseInt(maxResults), Integer.parseInt(firstResults)
                )
        );
    }

    @Override
    public Deployment get(String deploymentId) {
        return databaseConnectionService.getJdbcTemplate().queryForObject(
                "select * from bpmlight.deployment where deploymentId=?", deploymentMapper, deploymentId);
    }

    class DeploymentMapper implements RowMapper<Deployment> {
        @Override
        public Deployment mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Deployment deployment = new Deployment();
            deployment.setDate(rs.getTimestamp("created"));
            deployment.setDeploymentId(rs.getString("deploymentId"));
            deployment.setXml(rs.getString("xml"));
            deployment.setFileName(rs.getString("fileName"));
            deployment.setDeploymentKey(rs.getString("deploymentKey"));
            return deployment;
        }
    }
}
