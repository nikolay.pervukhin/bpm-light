package net.pervukhin.bpmlight.exporter.clickhouse.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.clickhouse.ClickhouseConnectionService;
import net.pervukhin.bpmlight.model.LifecycleType;
import net.pervukhin.bpmlight.model.Process;
import net.pervukhin.bpmlight.repository.ProcessRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service("processRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "clickhouse")
public class ProcessRepository implements ProcessRepositoryInt {
    @Autowired
    private ClickhouseConnectionService databaseConnectionService;

    private final ProcessMapper processMapper = new ProcessMapper();

    @Override
    public PaginationContainerDTO<Process> getList(boolean isActive, String maxResults, String firstResults) {
        return new PaginationContainerDTO<>(
                databaseConnectionService.getJdbcTemplate().queryForObject(
                        "SELECT COUNT() FROM (" + query(isActive) + ")"
                        , Integer.class),
                databaseConnectionService.getJdbcTemplate().query(
                        query(isActive)
                                + " ORDER BY created LIMIT ? OFFSET ?",
                        processMapper, Integer.valueOf(maxResults), Integer.valueOf(firstResults)
                )
        );
    }

    @Override
    public Process get(String processInstanceId) {
        return databaseConnectionService.getJdbcTemplate().queryForObject(
                "select * from bpmlight.process where processInstanceId=?", processMapper);
    }

    private String query(Boolean isActive) {
        return "select p1.created, p1.processDefinitionKey, p1.processDefinitionId, p1.businessKey,\n"
                + "       p1.processInstanceId, p1.superProcessInstanceId,\n"
                + "       if(not p2.endDate is null, p2.lifecycleType, p1.lifecycleType) as lifecycleType, "
                + "p2.endDate\n"
                + "from bpmlight.process p1\n"
                + "left join bpmlight.process p2 on p1.processInstanceId=p2.processInstanceId and p2.lifecycleType='ENDED'\n"
                + "where p1.lifecycleType='STARTED' "
                + (isActive ? "and p2.endDate is null " : "and not p2.endDate is null ");
    }

    class ProcessMapper implements RowMapper<Process> {
        @Override
        public Process mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Process process = new Process();
            process.setDate(rs.getTimestamp("created"));
            process.setProcessDefinitionKey(rs.getString("processDefinitionKey"));
            process.setProcessDefinitionId(rs.getString("processDefinitionId"));
            process.setBusinessKey(rs.getString("businessKey"));
            process.setProcessInstanceId(rs.getString("processInstanceId"));
            process.setSuperProcessInstanceId(rs.getString("superProcessInstanceId"));
            process.setLifecycleType(LifecycleType.valueOf(rs.getString("lifecycleType")));
            process.setEndDate(rs.getTimestamp("p2.endDate"));
            return process;
        }
    }
}
