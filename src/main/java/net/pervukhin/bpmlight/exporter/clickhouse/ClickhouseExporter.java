package net.pervukhin.bpmlight.exporter.clickhouse;

import net.pervukhin.bpmlight.exporter.ExporterInterface;
import net.pervukhin.bpmlight.model.Activity;
import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.model.ExportRecord;
import net.pervukhin.bpmlight.model.ExportRecordType;
import net.pervukhin.bpmlight.model.Model;
import net.pervukhin.bpmlight.model.Process;
import net.pervukhin.bpmlight.model.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;

@Service("exporter")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "clickhouse")
public class ClickhouseExporter implements ExporterInterface {
    private static final Logger logger = LoggerFactory.getLogger(ClickhouseExporter.class);

    @Autowired
    private ClickhouseConnectionService databaseConnectionService;

    @Override
    public void init() {
        databaseConnectionService.init();
    }

    @Override
    public void export(ExportRecordType exportRecordType, BlockingQueue<ExportRecord> records) {
        String queryString;
        String valuesString;
        final List parameters = new ArrayList();
        int itemsAdded = 0;
        switch (exportRecordType) {
            case MODEL:
                queryString = "insert into bpmlight.model (created,deploymentKey,fileName) values ";
                valuesString = "(?,?,?)";
                break;
            case DEPLOYMENT:
                queryString = "insert into bpmlight.deployment (created,deploymentId,xml,fileName,deploymentKey) "
                        + "values ";
                valuesString = "(?,?,?,?,?)";
                break;
            case PROCESS:
                queryString = "insert into bpmlight.process "
                        + "(created, processDefinitionKey, processDefinitionId, businessKey, processInstanceId, "
                        + "superProcessInstanceId, lifecycleType,endDate) values ";
                valuesString = "(?,?,?,?,?,?,?,?)";
                break;
            case ACTIVITY:
                queryString = "insert into bpmlight.activity "
                        + "(created, processInstanceId, activityId, activityInstanceId, lifecycleType,endDate) "
                        + "values ";
                valuesString = "(?,?,?,?,?,?)";
                break;
            case VARIABLE:
                queryString = "insert into bpmlight.variable (created, processInstanceId, variableName, textValue, "
                        + "serializerName) values ";
                valuesString = "(?,?,?,?,?)";
                break;
            default:
                throw new UnsupportedOperationException("ExportType doesnt implemented: " + exportRecordType);
        }
        while (! records.isEmpty()) {
            final ExportRecord recordElement = records.poll();
            itemsAdded++;
            switch (exportRecordType) {
                case MODEL:
                    Model model = (Model) recordElement;
                    parameters.add(model.getDate().getTime());
                    parameters.add(model.getDeploymentKey());
                    parameters.add(model.getFileName());
                    break;
                case DEPLOYMENT:
                    Deployment deployment = (Deployment) recordElement;
                    parameters.add(deployment.getDate().getTime());
                    parameters.add(deployment.getDeploymentId());
                    parameters.add(deployment.getXml());
                    parameters.add(deployment.getFileName());
                    parameters.add(deployment.getDeploymentKey());
                    break;
                case PROCESS:
                    Process process = (Process) recordElement;
                    parameters.add(process.getDate() != null ? process.getDate().getTime() : null);
                    parameters.add(process.getProcessDefinitionKey());
                    parameters.add(process.getProcessDefinitionId());
                    parameters.add(process.getBusinessKey());
                    parameters.add(process.getProcessInstanceId());
                    parameters.add(process.getSuperProcessInstanceId());
                    parameters.add(process.getLifecycleType().name());
                    parameters.add(process.getEndDate() != null ? process.getEndDate().getTime() : null);
                    break;
                case ACTIVITY:
                    Activity activity = (Activity) recordElement;
                    parameters.add(activity.getDate() != null ? activity.getDate().getTime() : null);
                    parameters.add(activity.getProcessInstanceId());
                    parameters.add(activity.getActivityId());
                    parameters.add(activity.getActivityInstanceId());
                    parameters.add(activity.getLifecycleType().name());
                    parameters.add(activity.getEndDate() != null ? activity.getEndDate().getTime() : null);
                    break;
                case VARIABLE:
                    Variable variable = (Variable) recordElement;
                    parameters.add(variable.getDate().getTime());
                    parameters.add(variable.getProcessInstanceId());
                    parameters.add(variable.getVariableName());
                    parameters.add(variable.getTextValue());
                    parameters.add(variable.getSerializerName());
                    break;
                default:
            }
        }
        if (itemsAdded > 0 && ! parameters.isEmpty()) {
            String query = queryString + (valuesString + ",").repeat(itemsAdded);
            query = query.substring(0, query.length() - 1);
            execute(query, parameters);
        }
    }

    private void execute(String query, List<Object> parameters) {
        try (PreparedStatement statement = databaseConnectionService.getConnection()
                .prepareStatement(query)) {
            for (int i=0; i<parameters.size(); i++) {
                if (parameters.get(i) instanceof String) {
                    statement.setString(i+1, String.valueOf(parameters.get(i)));
                }
                if (parameters.get(i) instanceof Long) {
                    statement.setLong(i+1, (Long) parameters.get(i));
                }
                if (parameters.get(i) instanceof Integer) {
                    statement.setInt(i+1, (Integer) parameters.get(i));
                }
                if (parameters.get(i) instanceof Double) {
                    statement.setDouble(i+1, (Double) parameters.get(i));
                }
                if (parameters.get(i) instanceof Date) {
                    statement.setTime(i+1, convertToTime((Date) parameters.get(i)));
                }
                if (parameters.get(i) == null) {
                    statement.setString(i+1, null);
                }
            }
            statement.execute();

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private java.sql.Time convertToTime(Date date) {
        return new java.sql.Time(date.getTime());
    }

    @Override
    public void destroy() {
        databaseConnectionService.close();
    }
}
