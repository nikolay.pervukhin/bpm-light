package net.pervukhin.bpmlight.exporter.clickhouse.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.clickhouse.ClickhouseConnectionService;
import net.pervukhin.bpmlight.model.Activity;
import net.pervukhin.bpmlight.model.LifecycleType;
import net.pervukhin.bpmlight.repository.ActivityRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service("activityRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "clickhouse")
public class ActivityRepository implements ActivityRepositoryInt {
    @Autowired
    private ClickhouseConnectionService databaseConnectionService;

    private final ActivityMapper activityMapper = new ActivityMapper();

    @Override
    public PaginationContainerDTO<Activity> getList(String processInstanceId, String maxResults, String firstResults) {
        return new PaginationContainerDTO<>(
                databaseConnectionService.getJdbcTemplate().queryForObject(
                        "select count() from (" + getQuery() + ")", Integer.class, processInstanceId),
                databaseConnectionService.getJdbcTemplate().query(
                        getQuery() + " order by created limit ? offset ?",
                        activityMapper, processInstanceId, Integer.parseInt(maxResults), Integer.parseInt(firstResults)
                )
        );
    }

    private String getQuery() {
        return "select p1.created, p1.processInstanceId, p1.activityId, p1.activityInstanceId,\n"
                + "       if(not p2.endDate is null, p2.lifecycleType, p1.lifecycleType) as lifecycleType, p2.endDate\n"
                + "from bpmlight.activity p1\n"
                + "left join bpmlight.activity p2 on p1.activityInstanceId=p2.activityInstanceId "
                + "and p2.lifecycleType='ENDED'\n"
                + "where p1.lifecycleType='STARTED' and p1.processInstanceId=?";
    }

    class ActivityMapper implements RowMapper<Activity> {
        @Override
        public Activity mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Activity activity = new Activity();
            activity.setDate(rs.getTimestamp("created"));
            activity.setProcessInstanceId(rs.getString("processInstanceId"));
            activity.setActivityId(rs.getString("activityId"));
            activity.setActivityInstanceId(rs.getString("activityInstanceId"));
            activity.setLifecycleType(LifecycleType.valueOf(rs.getString("lifecycleType")));
            activity.setEndDate(rs.getTimestamp("p2.endDate"));
            return activity;
        }
    }
}
