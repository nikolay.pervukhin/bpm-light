package net.pervukhin.bpmlight.exporter.h2.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.h2.H2ConnectionService;
import net.pervukhin.bpmlight.model.Model;
import net.pervukhin.bpmlight.repository.ModelRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service("modelRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "h2")
public class ModelRepository implements ModelRepositoryInt {
    @Autowired
    private H2ConnectionService databaseConnectionService;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public PaginationContainerDTO<Model> getList(String maxResults, String firstResults) {
        return new PaginationContainerDTO<>(
                databaseConnectionService.getJdbcTemplate().queryForObject(
                        "SELECT COUNT(*) FROM MODEL ", Integer.class),
                databaseConnectionService.getJdbcTemplate().query(
                        "SELECT * FROM MODEL ORDER BY created LIMIT ? OFFSET ?",
                        modelMapper, maxResults, firstResults
                )
        );
    }

    class ModelMapper implements RowMapper<Model> {
        @Override
        public Model mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Model model = new Model();
            model.setDate(rs.getTimestamp("CREATED"));
            model.setDeploymentKey(rs.getString("DEPLOYMENTKEY"));
            model.setFileName(rs.getString("FILENAME"));
            return model;
        }
    }
}
