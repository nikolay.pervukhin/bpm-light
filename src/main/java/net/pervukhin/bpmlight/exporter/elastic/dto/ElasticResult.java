package net.pervukhin.bpmlight.exporter.elastic.dto;

import net.pervukhin.bpmlight.model.ExportRecord;

public class ElasticResult<T extends ExportRecord> {
    private HitsResult<T> hits;

    public HitsResult<T> getHits() {
        return hits;
    }

    public void setHits(HitsResult<T> hits) {
        this.hits = hits;
    }
}
