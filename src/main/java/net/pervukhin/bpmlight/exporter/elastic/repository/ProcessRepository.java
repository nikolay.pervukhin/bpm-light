package net.pervukhin.bpmlight.exporter.elastic.repository;

import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.exporter.elastic.dto.ElasticGetOne;
import net.pervukhin.bpmlight.exporter.elastic.dto.ElasticResult;
import net.pervukhin.bpmlight.exporter.elastic.dto.HitsContainer;
import net.pervukhin.bpmlight.model.Process;
import net.pervukhin.bpmlight.repository.ProcessRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Collectors;

@Service("processRepository")
@ConditionalOnProperty(prefix = "exporter", name = "type", havingValue = "elastic")
public class ProcessRepository implements ProcessRepositoryInt {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${rest.process.list.activeUrl}")
    private String processListActiveUrl;

    @Value("${rest.process.list.completedUrl}")
    private String processListCompletedUrl;

    @Value("{rest.process.getOneUrl}")
    private String processGetOneUrl;

    public PaginationContainerDTO<Process> getList(boolean isActive, String maxResults, String firstResults) {
        ElasticResult<Process> result = restTemplate.exchange(
                (isActive ? processListActiveUrl : processListCompletedUrl)
                        .replace("{size}", maxResults)
                        .replace("{from}", firstResults),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ElasticResult<Process>>() {}).getBody();

        return result != null
            ? new PaginationContainerDTO<>(
                result.getHits().getTotal().getValue(),
                result.getHits().getHits().stream()
                        .map(HitsContainer::getSource)
                        .collect(Collectors.toList()))
            : null;
    }

    public Process get(String processInstanceId) {
        ElasticGetOne<Process> result = restTemplate.exchange(
                processGetOneUrl
                        .replace("{id}", processInstanceId),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ElasticGetOne<Process>>() {}).getBody();
        return result != null ? result.getSource() : null;
    }
}
