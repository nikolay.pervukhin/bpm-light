package net.pervukhin.bpmlight.exporter.elastic.dto;

public class TotalValue {
    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
