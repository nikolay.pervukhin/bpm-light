package net.pervukhin.bpmlight.exporter.elastic.dto.aggregation;

import net.pervukhin.bpmlight.model.ExportRecord;

import java.util.List;

public class Aggregated <T extends ExportRecord> {
    private List<Bucket<T>> buckets;

    public List<Bucket<T>> getBuckets() {
        return buckets;
    }

    public void setBuckets(List<Bucket<T>> buckets) {
        this.buckets = buckets;
    }
}
