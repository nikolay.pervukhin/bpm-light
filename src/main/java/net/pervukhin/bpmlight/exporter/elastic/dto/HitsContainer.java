package net.pervukhin.bpmlight.exporter.elastic.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.pervukhin.bpmlight.model.ExportRecord;

public class HitsContainer<T extends ExportRecord> {
    @JsonProperty("_id")
    private String id;

    @JsonProperty("_version")
    private Integer version;

    @JsonProperty("_source")
    private T source;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public T getSource() {
        return source;
    }

    public void setSource(T source) {
        this.source = source;
    }
}
