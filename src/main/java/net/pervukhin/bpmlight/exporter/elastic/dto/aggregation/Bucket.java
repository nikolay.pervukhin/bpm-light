package net.pervukhin.bpmlight.exporter.elastic.dto.aggregation;

import net.pervukhin.bpmlight.exporter.elastic.dto.HitsResult;
import net.pervukhin.bpmlight.model.ExportRecord;

public class Bucket <T extends ExportRecord> {
    private String key;
    private HitsResult<T> fields;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public HitsResult<T> getFields() {
        return fields;
    }

    public void setFields(HitsResult<T> fields) {
        this.fields = fields;
    }
}
