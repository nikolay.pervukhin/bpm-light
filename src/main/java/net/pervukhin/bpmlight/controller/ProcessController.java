package net.pervukhin.bpmlight.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.pervukhin.bpmlight.model.Process;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.repository.ProcessRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/process")
@Api(description = "Данные об экземплярах процессов")
public class ProcessController {
    @Autowired
    @Qualifier("processRepository")
    private ProcessRepositoryInt processRepository;

    @GetMapping("")
    @ApiOperation("Получить список экземпляров")
    public PaginationContainerDTO<Process> getList(
            @ApiParam("Активные или выполненные процессы")
            @RequestParam(required = false, defaultValue = "false") Boolean isActive,

            @ApiParam("Пагинация, кол-во элементов при выдаче")
            @RequestParam(required = false, defaultValue = "10") String maxResults,

            @ApiParam("Пагинация, начинать с элемента")
            @RequestParam(required = false, defaultValue = "0")
                    String firstResults) {

        return processRepository.getList(isActive, maxResults, firstResults);
    }

    @GetMapping("/{processInstanceId}")
    @ApiOperation("Получить все данные экземпляра")
    public Process getOne(
            @ApiParam("Идентификатор экземпляра")
            @RequestParam String processInstanceId) {
        return processRepository.get(processInstanceId);
    }
}
