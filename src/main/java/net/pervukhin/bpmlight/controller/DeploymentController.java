package net.pervukhin.bpmlight.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.repository.DeploymentRepositoryInt;
import net.pervukhin.bpmlight.service.DeploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deployment")
@Api(description = "Данные об установках процессов")
public class DeploymentController {
    @Autowired
    @Qualifier("deploymentRepository")
    private DeploymentRepositoryInt deploymentRepository;

    @Autowired
    private DeploymentService deploymentService;

    @GetMapping("")
    @ApiOperation("Получить список установок")
    public PaginationContainerDTO<Deployment> getList(
            @ApiParam("Идентификатор установки процесса")
            @RequestParam(required = true) String deploymentKey,

            @ApiParam("Пагинация, кол-во элементов при выдаче")
            @RequestParam(required = false, defaultValue = "10") String maxResults,

            @ApiParam("Пагинация, начинать с элемента")
            @RequestParam(required = false, defaultValue = "0") String firstResults) {

        return deploymentRepository.getList(deploymentKey, maxResults, firstResults);
    }

    @PostMapping("")
    @ApiOperation("Установить новый процесс")
    public void install(
            @ApiParam("Данные процесса")
            @RequestBody Deployment deployment) {
        deploymentService.deploy(deployment);
    }

    @GetMapping("/{deploymentId}")
    @ApiOperation("Получить выбранную установку")
    public Deployment getOne(
            @ApiParam("Идентификатор установки")
            @PathVariable String deploymentId) {
        return deploymentRepository.get(deploymentId);
    }
}
