package net.pervukhin.bpmlight.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.pervukhin.bpmlight.model.Variable;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;
import net.pervukhin.bpmlight.repository.VariableRepositoryInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/variable")
@Api(description = "Данные о переменных экземпляра процесса")
public class VariableController {
    @Autowired
    @Qualifier("variableRepository")
    private VariableRepositoryInt variableRepository;

    @GetMapping("")
    @ApiOperation("Получить список переменных")
    public PaginationContainerDTO<Variable> getList(
            @ApiParam("Идентификатор экземпляра процесса")
            @RequestParam(required = false) String processInstanceId,

            @ApiParam("Пагинация, кол-во элементов при выдаче")
            @RequestParam(required = false, defaultValue = "10") String maxResults,

            @ApiParam("Пагинация, начинать с элемента")
            @RequestParam(required = false, defaultValue = "0")
                    String firstResults) {

        return variableRepository.getList(processInstanceId, maxResults, firstResults);
    }
}
