package net.pervukhin.bpmlight.repository;

import net.pervukhin.bpmlight.model.Deployment;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;

public interface DeploymentRepositoryInt {
    PaginationContainerDTO<Deployment> getList(String deploymentKey, String maxResults, String firstResults);
    Deployment get(String deploymentId);

}
