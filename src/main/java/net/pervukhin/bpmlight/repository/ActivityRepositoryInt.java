package net.pervukhin.bpmlight.repository;

import net.pervukhin.bpmlight.model.Activity;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;

public interface ActivityRepositoryInt {
    PaginationContainerDTO<Activity> getList(String processInstanceId, String maxResults, String firstResults);
}
