package net.pervukhin.bpmlight.repository;

import net.pervukhin.bpmlight.model.Process;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;

public interface ProcessRepositoryInt {
    PaginationContainerDTO<Process> getList(boolean isActive, String maxResults, String firstResults);
    Process get(String processInstanceId);
}
