package net.pervukhin.bpmlight.repository;

import net.pervukhin.bpmlight.model.Variable;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;

public interface VariableRepositoryInt {
    PaginationContainerDTO<Variable> getList(String processInstanceId, String maxResults, String firstResults);
}
