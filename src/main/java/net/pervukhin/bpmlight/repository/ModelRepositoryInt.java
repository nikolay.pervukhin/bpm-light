package net.pervukhin.bpmlight.repository;

import net.pervukhin.bpmlight.model.Model;
import net.pervukhin.bpmlight.dto.PaginationContainerDTO;

public interface ModelRepositoryInt {
    PaginationContainerDTO<Model> getList(String maxResults, String firstResults);
}
